#include "cBulletRigidBody.h"
#include "BulletShapes.h"
#include "nConvert.h"

namespace nPhysics
{
	cBulletRigidBody::cBulletRigidBody(const sRigidBodyDesc& desc, iShape* shape)
		: shape(shape)
		, position(desc.Position)
		, velocity(desc.Velocity)
		, mass(desc.Mass)
		, rotation(desc.Rotation)
		, isStatic(false)
		, acceleration(glm::vec3(0,0,0))
		, orientationVelocity(glm::quat())
		, colliding(false)
		
	{
		/*orientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

		btQuaternion quatRot(orientation.x, orientation.y, orientation.z, orientation.w);
		btTransform worldTransform;
		worldTransform.setIdentity();
		worldTransform.setOrigin(btVector3(desc.Position.x, desc.Position.y, desc.Position.z));
		worldTransform.setRotation(quatRot);
		motionState = new btDefaultMotionState(worldTransform);

		btVector3 inertia(0.0f, 0.0f, 0.0f);
		float mass = desc.Mass;

		if (shape->GetShapeType() == SHAPE_TYPE_PLANE)
		{
			mass = 0.0f;
		}

		((cBulletSphereShape*)shape)->bulletShape->calculateLocalInertia(mass, inertia);
		this->collisionShape = ((cBulletPlaneShape*)shape)->bulletShape;

		if (shape->GetShapeType() == SHAPE_TYPE_PLANE)
		{
			btRigidBody::btRigidBodyConstructionInfo info(mass, motionState, ((cBulletPlaneShape*)shape)->bulletShape, inertia);
			info.m_localInertia = inertia;
			info.m_restitution = 0.6f;
			body = new btRigidBody(info);
		}
		else
		{
			btRigidBody::btRigidBodyConstructionInfo info(mass, motionState, ((cBulletSphereShape*)shape)->bulletShape, inertia);
			info.m_localInertia = inertia;
			info.m_restitution = 0.6f;
			body = new btRigidBody(info);
		}
		
		body->setLinearVelocity(btVector3(desc.Velocity.x, desc.Velocity.y, desc.Velocity.z));*/


		//btCollisionShape* colShape = new btBoxShape(btVector3(1,1,1));
		orientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));
		btCollisionShape* colShape = 0;
		if (shape->GetShapeType() == SHAPE_TYPE_PLANE)
		{
			isStatic = true;
			//glm::vec3 normal;
			/*(cBulletPlaneShape*)shape)->
			(cBulletPlaneShape*)shape)->GetPlaneNormal(normal);
			colShape = cBulletPlaneShape(, planeConst, pointOnPlane);*/

			glm::vec3 planeConst = desc.Position + ((cBulletPlaneShape*)shape)->normal * 30.0f;

			((cBulletPlaneShape*)shape)->bulletShape = 
				new btStaticPlaneShape(nConvert::ToBullet(((cBulletPlaneShape*)shape)->normal), 0);

			colShape = ((cBulletPlaneShape*)shape)->bulletShape;

			//(cBulletPlaneShape*)shape)->bulletShape = new btStaticPlaneShape(nConvert::ToBullet((cBulletPlaneShape*)shape)->)), glm::dot(normal, pointOnPlane));
		}
		else
		{
			colShape = ((cBulletSphereShape*)shape)->bulletShape;
		}
		//btCollisionShape* colShape = new btSphereShape(btScalar(1.));  //TODO: use your iShape!

		/// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setOrigin(nConvert::ToBullet(desc.Position));
		//TODO set other things like rotation and such and such and whathaveyou
		startTransform.setIdentity();

		btScalar mass(desc.Mass);

		//maybe use shape to tell about this one
		if (mass == 0.f || isStatic)
		{
			mass = 0.0f;
			isStatic = true;
		}

		btVector3 localInertia(0, 0, 0);
		if (!isStatic)
			colShape->calculateLocalInertia(mass, localInertia);

		//startTransform.setOrigin(btVector3(2, 10, 0));
		startTransform.setOrigin(btVector3(desc.Position.x, desc.Position.y, desc.Position.z));

		//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
		motionState = new btDefaultMotionState(startTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, colShape, localInertia);
		body = new btRigidBody(rbInfo);

		body->setUserPointer(this);

	}
	cBulletRigidBody::~cBulletRigidBody()
	{
		body->setUserPointer(0);
		delete body;
		delete motionState;
		shape = 0;
	}

	iShape* cBulletRigidBody::GetShape()
	{
		return shape;
	}

	/*void cBulletRigidBody::GetTransform(glm::mat4& transformOut)
	{
		transformOut = glm::mat4_cast(rotation);
		transformOut[3][0] = position.x;
		transformOut[3][1] = position.y;
		transformOut[3][2] = position.z;
		transformOut[3][3] = 1.f;
	}*/
	void cBulletRigidBody::GetPosition(glm::vec3& positionOut)
	{
		//positionOut = position;
		nConvert::ToGlm(body->getCenterOfMassPosition(), positionOut);
	}
	void cBulletRigidBody::GetRotation(glm::vec3& rotationOut)
	{
		rotationOut = glm::eulerAngles(rotation);
	}
	void cBulletRigidBody::SetStatic(bool isStat)
	{
		isStatic = isStat;
	}
	void cBulletRigidBody::GetVelocity(glm::vec3 & velocityOut)
	{
		velocityOut = nConvert::ToGlm(this->body->getLinearVelocity());
	}
	void cBulletRigidBody::SetVelocity(glm::vec3 & velocityIn)
	{
		body->activate(true);
		this->body->setLinearVelocity(nConvert::ToBullet(velocityIn));
	}
	void cBulletRigidBody::Push(glm::vec3 accel)
	{
		//this->acceleration = accel;
		//body->applyImpulse(nConvert::ToBullet(accel),btVector3(0,0,0));
		body->activate(true);
		body->applyCentralForce(nConvert::ToBullet(accel));
	}
	void cBulletRigidBody::GetOrientation(glm::quat & orientation)
	{
		//orientation = this->orientation;
		orientation = nConvert::ToGlm(this->body->getOrientation());
	}
	void cBulletRigidBody::GetColliding(bool & colliding)
	{
		colliding = this->colliding;
	}

	void cBulletRigidBody::GetTransform(glm::mat4& transformOut)
	{
		nConvert::ToGlm(body->getWorldTransform(), transformOut);
	}
}
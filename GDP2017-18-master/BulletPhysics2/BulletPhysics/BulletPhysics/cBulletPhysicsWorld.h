#pragma once
#include <iPhysicsWorld.h>
#include <vector>
#include "cBulletRigidBody.h"
#include "cBulletIntegrator.h"
//#include "BulletInverseDynamics/IDConfigBuiltin.hpp"
//#include "bt"
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

namespace nPhysics
{
	class cBulletPhysicsWorld : public iPhysicsWorld
	{
	public:
		virtual ~cBulletPhysicsWorld();
		cBulletPhysicsWorld();
		cBulletPhysicsWorld(btVector3 gravity);
		cBulletPhysicsWorld(glm::vec3 max, glm::vec3 min);

		virtual void TimeStep(float deltaTime);

		virtual void AddRigidBody(iRigidBody* rigidBody);
		virtual void RemoveRigidBody(iRigidBody* rigidBody);

		virtual void SetDebugRenderer(iDebugRenderer* debugRenderer);
		virtual void RenderDebug();
		virtual void Collide(iRigidBody* first, iRigidBody* second);
		virtual void CollideSphereSphere(iRigidBody* first, iRigidBody* second);
		virtual void CollideSpherePlane(iRigidBody* first, iRigidBody* second);

	private:
		
		std::vector<cBulletRigidBody*> rigidBodies;
		float deltaTime;
		cBulletIntegrator integrator;
		double totalTime;

		glm::vec3 worldMax;
		glm::vec3 worldMin;

		btDiscreteDynamicsWorld* dynamicsWorld;
		btAlignedObjectArray<btCollisionShape*> collisionShapes;
		btSequentialImpulseConstraintSolver* solver;
		btBroadphaseInterface* overlappingPairCache;
		btCollisionDispatcher* dispatcher;
		btDefaultCollisionConfiguration* collisionConfiguration;
		btCollisionWorld* collisionWorld;
		//btBroadphaseInterface* broadPhase;
	};
}
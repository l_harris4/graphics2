// This file is used to laod the models
#include "cGameObject.h"
#include <vector>
#include "Utilities.h"		// getRandInRange()
#include <glm/glm.hpp>
#include "globalGameStuff.h"

// For the cSimpleAssimpSkinnedMeshLoader class
#include "assimp/cSimpleAssimpSkinnedMeshLoader_OneMesh.h"

#include "cAnimationState.h"

extern std::vector< cGameObject* >  g_vecGameObjects;
extern cGameObject* g_pTheDebugSphere;

extern nPhysics::iPhysicsFactory* gPhysicsFactory;
extern nPhysics::iPhysicsWorld* gPhysicsWorld;

void LoadModelsIntoScene(void)
{

	{	// Skinned mesh  model
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "Justin";
		// This assigns the game object to the particular skinned mesh type 
		pTempGO->pSimpleSkinnedMesh = ::g_pRPGSkinnedMesh;
		// Add a default animation 
		pTempGO->pAniState = new cAnimationState();
		pTempGO->pAniState->defaultAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Idle(FBX2013).FBX";
		pTempGO->pAniState->defaultAnimation.frameStepTime = 0.005f;
		// Get the total time of the entire animation
		pTempGO->pAniState->defaultAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->GetDuration();

		cPhysicalProperties physState;
		physState.position = glm::vec3(0.0f, 30.0, 0.0f);
		//physState.setOrientationEulerAngles(glm::vec3(90, 90, 90));
		pTempGO->SetPhysState(physState);
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 0.25f;
		meshInfo.setMeshOrientationEulerAngles(glm::vec3(0.0f, 0.0f, 0.0f));
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
		meshInfo.bDrawAsWireFrame = false;
		meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("GuysOnSharkUnicorn.bmp", 1.0f));
		meshInfo.name = ::g_pRPGSkinnedMesh->friendlyName;
		pTempGO->vecMeshes.push_back(meshInfo);


		//add the physics body
		nPhysics::iShape* shape;
		nPhysics::sRigidBodyDesc desc;
		desc.Position = pTempGO->getPosition();
		shape = gPhysicsFactory->CreateSphere(20);
		pTempGO->rBodyOffset = glm::vec3(0, -25, 0);
		pTempGO->rBody = gPhysicsFactory->CreateRigidBody(desc, shape);
		pTempGO->rBody->SetStatic(true);
		::gPhysicsWorld->AddRigidBody(pTempGO->rBody);
		pTempGO->physics = true;


		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}



	{	// Skinned mesh  model
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "Justin2";
		// This assigns the game object to the particular skinned mesh type 
		pTempGO->pSimpleSkinnedMesh = ::g_pRPGSkinnedMesh2;
		// Add a default animation 
		pTempGO->pAniState = new cAnimationState();
		pTempGO->pAniState->defaultAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Idle(FBX2013).FBX";
		pTempGO->pAniState->defaultAnimation.frameStepTime = 0.005f;
		// Get the total time of the entire animation
		pTempGO->pAniState->defaultAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->GetDuration();

		cPhysicalProperties physState;
		physState.position = glm::vec3(40.0f, 0.0, 0.0f);
		//physState.setOrientationEulerAngles(glm::vec3(90, 90, 90));
		pTempGO->SetPhysState(physState);
		sMeshDrawInfo meshInfo;

		meshInfo.scale = 0.25f;
		meshInfo.setMeshOrientationEulerAngles(glm::vec3(0.0f, 0.0f, 0.0f));
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
		meshInfo.bDrawAsWireFrame = true;
		meshInfo.name = ::g_pRPGSkinnedMesh2->friendlyName;
		pTempGO->vecMeshes.push_back(meshInfo);

		//add the physics body
		nPhysics::iShape* shape;
		nPhysics::sRigidBodyDesc desc;
		desc.Position = pTempGO->getPosition();
		shape = gPhysicsFactory->CreateSphere(20);
		pTempGO->rBodyOffset = glm::vec3(0, -25, 0);
		pTempGO->rBody = gPhysicsFactory->CreateRigidBody(desc, shape);
		pTempGO->rBody->SetStatic(true);
		::gPhysicsWorld->AddRigidBody(pTempGO->rBody);
		pTempGO->physics = true;

		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}



	{	
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "Ground";

		cPhysicalProperties physState;
		physState.position = glm::vec3(0.0f, 0.0, 0.0f);
		physState.setOrientationEulerAngles(glm::vec3(0, 90, 0), true);
		pTempGO->SetPhysState(physState);
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 40.0f;
		meshInfo.debugDiffuseColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		meshInfo.name = "lava3.ply";
		meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("GuysOnSharkUnicorn.bmp", 1.0f));
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("Utah_Teapot_xyz_n_uv_Enterprise.bmp", 0.0f));
		pTempGO->vecMeshes.push_back(meshInfo);

		nPhysics::iShape* shape;
		glm::vec3 normal = glm::vec3(0, 1, 0);
		nPhysics::sRigidBodyDesc desc;
		desc.Position = pTempGO->getPosition();
		shape = gPhysicsFactory->CreatePlane(normal, 1);
		//shape = gPhysicsFactory->CreateBox(glm::vec3(200, 2, 200));

		pTempGO->rBody = gPhysicsFactory->CreateRigidBody(desc, shape);
		pTempGO->rBody->SetStatic(true);
		::gPhysicsWorld->AddRigidBody(pTempGO->rBody);

		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}

	

	{
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "security cam 2";

		cPhysicalProperties physState;
		physState.position = glm::vec3(0.0f, 30.0, 180.0);
		physState.setOrientationEulerAngles(glm::vec3(90, 0, 0), true);
		pTempGO->SetPhysState(physState);
		pTempGO->isSecurityCam2 = true;
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 25.0f;
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		meshInfo.name = "plainPlane.ply";
		meshInfo.bDisableBackFaceCulling = true;
		meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("ScreenCover2.bmp", 1.0f));
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("Utah_Teapot_xyz_n_uv_Enterprise.bmp", 0.0f));
		pTempGO->vecMeshes.push_back(meshInfo);

		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}

	{	
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "securityCam";

		cPhysicalProperties physState;
		physState.position = glm::vec3(80.0f, 30.0, 180.0f);
		physState.setOrientationEulerAngles(glm::vec3(-90, -180, 0), true);
		pTempGO->SetPhysState(physState);
		pTempGO->isSecurityCam = true;
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 5.0f;
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		meshInfo.name = "lava3.ply";
		meshInfo.bDisableBackFaceCulling = true;
		meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("ScreenCover.bmp", 1.0f));
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("Utah_Teapot_xyz_n_uv_Enterprise.bmp", 0.0f));
		pTempGO->vecMeshes.push_back(meshInfo);

		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}

	{	
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "xRay";

		cPhysicalProperties physState;
		physState.position = glm::vec3(-80.0f, 30.0, 180.0f);
		physState.setOrientationEulerAngles(glm::vec3(-90, 0, 0), true);
		pTempGO->SetPhysState(physState);
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 5.0f;
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		meshInfo.name = "lava3.ply";
		meshInfo.bDisableBackFaceCulling = true;
		meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("ScreenCover.bmp", 1.0f));
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("Utah_Teapot_xyz_n_uv_Enterprise.bmp", 0.0f));
		pTempGO->vecMeshes.push_back(meshInfo);

		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}


	{	
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "Sphere";

		cPhysicalProperties physState;
		physState.position = glm::vec3(20.0f, 30.0, 50.0);
		physState.setOrientationEulerAngles(glm::vec3(0, 0, 0), true);
		pTempGO->SetPhysState(physState);

		sMeshDrawInfo meshInfo;
		meshInfo.scale = 5.0f;
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		meshInfo.name = "SmoothSphere";
		meshInfo.bIsEnvirMapped = true;
		meshInfo.reflectBlendRatio = 0.5f;
		meshInfo.refractBlendRatio = 0.5f;
		meshInfo.coefficientRefract = 0.1f;
		meshInfo.bDisableBackFaceCulling = true;
		meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("GuysOnSharkUnicorn.bmp", 1.0f));
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("Utah_Teapot_xyz_n_uv_Enterprise.bmp", 0.0f));
		pTempGO->vecMeshes.push_back(meshInfo);

		//add the physics body
		nPhysics::iShape* shape;
		nPhysics::sRigidBodyDesc desc;
		desc.Position = pTempGO->getPosition();
		shape = gPhysicsFactory->CreateSphere(meshInfo.scale);
		pTempGO->rBody = gPhysicsFactory->CreateRigidBody(desc, shape);
		pTempGO->rBody->SetStatic(true);
		::gPhysicsWorld->AddRigidBody(pTempGO->rBody);
		pTempGO->physics = true;

		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}

	{	// Skinned mesh  model
		cGameObject* pTempGO = new cGameObject();
		pTempGO->friendlyName = "Sphere";

		cPhysicalProperties physState;
		physState.position = glm::vec3(0.0f, 30.0, 100.0);
		physState.setOrientationEulerAngles(glm::vec3(0, 0, 0), true);
		pTempGO->SetPhysState(physState);

		sMeshDrawInfo meshInfo;
		meshInfo.scale = 5.0f;
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		meshInfo.name = "SmoothSphere";
		meshInfo.bIsEnvirMapped = true;
		meshInfo.reflectBlendRatio = 0.5f;
		meshInfo.refractBlendRatio = 0.5f;
		meshInfo.coefficientRefract = 0.1f;
		meshInfo.bDisableBackFaceCulling = true;
		meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("GuysOnSharkUnicorn.bmp", 1.0f));
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("Utah_Teapot_xyz_n_uv_Enterprise.bmp", 0.0f));
		pTempGO->vecMeshes.push_back(meshInfo);

		//add the physics body
		nPhysics::iShape* shape;
		nPhysics::sRigidBodyDesc desc;
		desc.Position = pTempGO->getPosition();
		shape = gPhysicsFactory->CreateSphere(meshInfo.scale);
		pTempGO->rBody = gPhysicsFactory->CreateRigidBody(desc, shape);
		pTempGO->rBody->SetStatic(true);
		::gPhysicsWorld->AddRigidBody(pTempGO->rBody);
		pTempGO->physics = true;

		::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
	}


	//meshInfo.bIsEnvirMapped = true;
	//meshInfo.reflectBlendRatio = 0.5f;
	//meshInfo.refractBlendRatio = 0.5f;
	//meshInfo.coefficientRefract = 0.1f;



	// Our skybox object
	{
		//cGameObject* pTempGO = new cGameObject();
		::g_pSkyBoxObject = new cGameObject();
		cPhysicalProperties physState;
		::g_pSkyBoxObject->SetPhysState(physState);
		sMeshDrawInfo meshInfo;
		meshInfo.scale = 10000.0f;
		meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		meshInfo.name = "SmoothSphereRadius1InvertedNormals";
		//meshInfo.vecMehs2DTextures.push_back(sTextureBindBlendInfo("GuysOnSharkUnicorn.bmp", 1.0f));
		meshInfo.vecMeshCubeMaps.push_back(sTextureBindBlendInfo("space", 1.0f));
		meshInfo.bIsSkyBoxObject = true;
		::g_pSkyBoxObject->vecMeshes.push_back(meshInfo);
		// IS SKYBOX
		::g_vecGameObjects.push_back(::g_pSkyBoxObject);		// Fastest way to add
	}

	return;
}

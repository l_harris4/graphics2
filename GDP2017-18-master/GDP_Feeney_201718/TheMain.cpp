// Include glad and GLFW in correct order
#include "globalOpenGL_GLFW.h"


#include <iostream>			// C++ cin, cout, etc.
//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective

#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>

#include <vector>		//  smart array, "array" in most languages
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "sMeshDrawInfo.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"


//#include "Physics/Physics.h"	// Physics collision detection functions
#include "Physics/cPhysicsWorld.h"

#include "cLightManager.h"

// Include all the things that are accessed in other files
#include "globalGameStuff.h"

#include "cCamera.h"

#include "cFBO.h" 

//**********
// BE a little careful of including this everywhere...
#include "assimp/cSimpleAssimpSkinnedMeshLoader_OneMesh.h"
//**********

// Here, the scene is rendered in 3 passes:
// 1. Render geometry to G buffer
// 2. Perform deferred pass, rendering to Deferred buffer
// 3. Then post-pass ("2nd pass" to the scree)
//    Copying from the Pass2_Deferred buffer to the final screen
cFBO g_FBO_Pass1_G_Buffer;
cFBO g_FBO_Pass2_Deferred;
cFBO g_FBO_Pass2_Deferred2;

//cFBO g_FBO_Pass1_G_Buffer2;

cFBO defferedTexture;
cFBO defferedTexture2;
//cFBO g_FBO_Pass2_Deferred2;

GLint sexyShaderID;
std::vector<cGameObject*> onlyTheFirstObject;
std::vector<cGameObject*> onlyTheFourthObject;
std::vector<cGameObject*> everythingButTheThirdObject;
std::vector<cGameObject*> everythingButTheFourthObject;
GLint renderPassNumber_LocID;



cGameObject* g_pSkyBoxObject = NULL;	// (theMain.cpp)


// Remember to #include <vector>...
std::vector< cGameObject* >  g_vecGameObjects;

cCamera* g_pTheCamera = NULL;


cVAOMeshManager* g_pVAOManager = 0;		// or NULL, or nullptr

cShaderManager*		g_pShaderManager = 0;		// Heap, new (and delete)
cLightManager*		g_pLightManager = 0;

CTextureManager*		g_pTextureManager = 0;

cModelAssetLoader*		g_pModelAssetLoader = NULL;


cDebugRenderer*			g_pDebugRenderer = 0;


// This contains the AABB grid for the terrain...
cAABBBroadPhase* g_terrainAABBBroadPhase = 0;

//cPhysicsWorld*	g_pPhysicsWorld = NULL;	// (theMain.cpp)

nPhysics::iPhysicsFactory* gPhysicsFactory;
nPhysics::iPhysicsWorld* gPhysicsWorld;


// This is used in the "render a previous pass" object
//cGameObject* g_ExampleTexturedQuad = NULL;

// For stencil buffer example...
//cGameObject* g_Room = NULL;
//cGameObject* g_RoomMaskForStencil = NULL;



#include "cFrameBuffer.h"

bool g_IsWindowFullScreen = false;
GLFWwindow* g_pGLFWWindow = NULL;


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

bool InitPhysics()
{
	gPhysicsFactory = new nPhysics::cBulletPhysicsFactory();
	gPhysicsWorld = gPhysicsFactory->CreateWorld();
	return true;
}


float g_ChromaticAberrationOffset = 0.0f;
float g_CR_Max = 0.1f;
double g_CA_CountDown = 0.0f;

int selectedGameObjectIndex = 0;

float textureOffset = 0.0f;
float textureOffsetGrowth = 0.01f;

float xOrbLoc = 0.0f;
float yOrbLoc = 0.0f;
float orbRadius = 0.1f;

float xOrbLoc2 = 0.5f;
float yOrbLoc2 = 0.5f;
float orbRadius2 = 0.1f;

float orbRadiusMax = 0.3f;
float orbRadiusRandMax = 0.5f;
float radiusStep = 0.009;
bool goingUp = true;

float orbRadiusRandMax2 = 0.5f;
bool goingUp2 = true;
bool drunkEffect = false;

const int RENDER_PASS_0_G_BUFFER_PASS = 0;
const int RENDER_PASS_1_DEFERRED_RENDER_PASS = 1;
const int RENDER_PASS_2_FULL_SCREEN_EFFECT_PASS = 2;
extern void setAnimationStates();

// Moved to GLFW_keyboardCallback.cpp
//static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)


int main(void)
{

	InitPhysics();
	//GLFWwindow* pGLFWWindow;		// Moved to allow switch from windowed to full-screen
	glfwSetErrorCallback(error_callback);



	if (!glfwInit())
	{
		// exit(EXIT_FAILURE);
		std::cout << "ERROR: Couldn't init GLFW, so we're pretty much stuck; do you have OpenGL??" << std::endl;
		return -1;
	}

	int height = 480;	/* default */
	int width = 640;	// default
	std::string title = "OpenGL Rocks";

	std::ifstream infoFile("config.txt");
	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}
	else
	{	// File DID open, so read it... 
		std::string a;

		infoFile >> a;	// "Game"	//std::cin >> a;
		infoFile >> a;	// "Config"
		infoFile >> a;	// "width"

		infoFile >> width;	// 1080

		infoFile >> a;	// "height"

		infoFile >> height;	// 768

		infoFile >> a;		// Title_Start

		std::stringstream ssTitle;		// Inside "sstream"
		bool bKeepReading = true;
		do
		{
			infoFile >> a;		// Title_End??
			if (a != "Title_End")
			{
				ssTitle << a << " ";
			}
			else
			{	// it IS the end! 
				bKeepReading = false;
				title = ssTitle.str();
			}
		} while (bKeepReading);


	}//if ( ! infoFile.is_open() )




	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// C++ string
	// C no strings. Sorry. char    char name[7] = "Michael\0";
	::g_pGLFWWindow = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!::g_pGLFWWindow)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(::g_pGLFWWindow, key_callback);
	// For the FBO to resize when the window changes
	glfwSetWindowSizeCallback(::g_pGLFWWindow, window_size_callback);

	glfwMakeContextCurrent(::g_pGLFWWindow);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);




	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	// General error string, used for a number of items during start up
	std::string error;

	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	//fragShader.fileName = "simpleFrag.glsl"; 
	fragShader.fileName = "simpleFragDeferred.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;


	// Triangle debug renderer test...
	::g_pDebugRenderer = new cDebugRenderer();
	if (!::g_pDebugRenderer->initialize(error))
	{
		std::cout << "Warning: couldn't init the debug renderer." << std::endl;
		std::cout << "\t" << ::g_pDebugRenderer->getLastError() << std::endl;
	}

	// Load models
	::g_pModelAssetLoader = new cModelAssetLoader();
	::g_pModelAssetLoader->setBasePath("assets/models/");

	::g_pVAOManager = new cVAOMeshManager();

	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");


	sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, ::g_pModelAssetLoader, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}

	LoadModelsIntoScene();


	// Named unifrom block
	// Now many uniform blocks are there? 
	GLint numberOfUniformBlocks = -1;
	glGetProgramiv(currentProgID, GL_ACTIVE_UNIFORM_BLOCKS, &numberOfUniformBlocks);

	// https://www.opengl.org/wiki/GLAPI/glGetActiveUniformBlock

	// Set aside some buffers for the names of the blocks
	const int BUFFERSIZE = 1000;

	int name_length_written = 0;

	char NUBName_0[BUFFERSIZE] = { 0 };
	char NUBName_1[BUFFERSIZE] = { 0 };

	glGetActiveUniformBlockName(currentProgID,
		0,
		BUFFERSIZE,
		&name_length_written,
		NUBName_0);

	glGetActiveUniformBlockName(currentProgID,
		1,
		BUFFERSIZE,
		&name_length_written,
		NUBName_1);

	//	NUB_lighting
	//	NUB_perFrame

	GLuint NUB_Buffer_0_ID = 0;
	GLuint NUB_Buffer_1_ID = 0;

	glGenBuffers(1, &NUB_Buffer_0_ID);
	glBindBuffer(GL_UNIFORM_BUFFER, NUB_Buffer_0_ID);

	glGenBuffers(1, &NUB_Buffer_1_ID);
	glBindBuffer(GL_UNIFORM_BUFFER, NUB_Buffer_1_ID);




	// Get the uniform locations for this shader
//	mvp_location = glGetUniformLocation(currentProgID, "MVP");		// program, "MVP");


//	GLint uniLoc_diffuseColour = glGetUniformLocation( currentProgID, "diffuseColour" );

	::g_pLightManager = new cLightManager();

	::g_pLightManager->CreateLights(10);	// There are 10 lights in the shader

	::g_pLightManager->vecLights[0].setLightParamType(cLight::POINT);
	::g_pLightManager->vecLights[0].position = glm::vec3(0.0f, 40.0f, 0.0f);
	::g_pLightManager->vecLights[0].attenuation.y = 0.009f;

	::g_pLightManager->vecLights[1].setLightParamType(cLight::POINT);
	::g_pLightManager->vecLights[1].position = glm::vec3(10.0f, 40.0f, 280.0f);
	::g_pLightManager->vecLights[1].attenuation.y = 0.003f;


	//::g_pLightManager->vecLights[1].position = glm::vec3(0.0f, 570.0f, -212.0f);
	//::g_pLightManager->vecLights[1].attenuation.y = 0.000456922280f;		//0.172113f;
	//::g_pLightManager->vecLights[1].setLightParamType(cLight::SPOT);		// <--- DOH! This would explain why I couldn't light up the scene!!
	//// Point spot straight down at the ground
	//::g_pLightManager->vecLights[1].direction = glm::vec3(0.0f, -1.0f, 0.0f);
	//::g_pLightManager->vecLights[1].setLightParamSpotPrenumAngleInner(glm::radians(15.0f));
	//::g_pLightManager->vecLights[1].setLightParamSpotPrenumAngleOuter(glm::radians(45.0f));
	//::g_pLightManager->vecLights[1].position = glm::vec3(0.0f, 50.0f, 0.0f);


	::g_pLightManager->LoadShaderUniformLocations(currentProgID);



	// Texture 
	::g_pTextureManager = new CTextureManager();

	std::cout << "GL_MAX_TEXTURE_IMAGE_UNITS: " << ::g_pTextureManager->getOpenGL_GL_MAX_TEXTURE_IMAGE_UNITS() << std::endl;
	std::cout << "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: " << ::g_pTextureManager->getOpenGL_GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS() << std::endl;

	::g_pTextureManager->setBasePath("assets/textures");
	if (!::g_pTextureManager->Create2DTextureFromBMPFile("Utah_Teapot_xyz_n_uv_Checkerboard.bmp", true))
	{
		std::cout << "Didn't load the texture. Oh no!" << std::endl;
	}
	else
	{
		std::cout << "Texture is loaded! Hazzah!" << std::endl;
	}
	::g_pTextureManager->Create2DTextureFromBMPFile("Utah_Teapot_xyz_n_uv_Enterprise.bmp", true);
	::g_pTextureManager->Create2DTextureFromBMPFile("GuysOnSharkUnicorn.bmp", true);
	::g_pTextureManager->Create2DTextureFromBMPFileNoFilter("ScreenCover.bmp", true);
	::g_pTextureManager->Create2DTextureFromBMPFileNoFilter("ScreenCover2.bmp", true);
	::g_pTextureManager->Create2DTextureFromBMPFile("barberton_etm_2001121_lrg.bmp", true);
	::g_pTextureManager->Create2DTextureFromBMPFile("height_map_norway-height-map-aster-30m.bmp", true);
	::g_pTextureManager->Create2DTextureFromBMPFile("A_height_map_norway-height-map-aster-30m.bmp", true);

	::g_pTextureManager->setBasePath("assets/textures/skybox");
	if (!::g_pTextureManager->CreateCubeTextureFromBMPFiles(
		"space",
		"SpaceBox_right1_posX.bmp",
		"SpaceBox_left2_negX.bmp",
		"SpaceBox_top3_posY.bmp",
		"SpaceBox_bottom4_negY.bmp",
		"SpaceBox_front5_posZ.bmp",
		"SpaceBox_back6_negZ.bmp", true, true))
	{
		std::cout << "Didn't load skybox" << std::endl;
	}



	::g_pTheCamera = new cCamera();

	::g_pTheCamera->setCameraMode(cCamera::MODE_FLY_USING_LOOK_AT);
	::g_pTheCamera->FlyCamLA->setEyePosition(glm::vec3(0.0f, 10.0f, 650.0f));
	::g_pTheCamera->FlyCamLA->setTargetInWorld(glm::vec3(0.0f, 20.0f, 0.0f));
	::g_pTheCamera->FlyCamLA->setUpVector(glm::vec3(0.0f, 1.0f, 0.0f));

	//	::g_pTheCamera->FollowCam->SetOrUpdateTarget(glm::vec3(1.0f));



	//::g_pPhysicsWorld = new cPhysicsWorld();


	// All loaded!
	std::cout << "And we're good to go! Staring the main loop..." << std::endl;

	glEnable(GL_DEPTH);

	GLint renderPassNumber_LocID = glGetUniformLocation(sexyShaderID, "renderPassNumber");
	// Create an FBO
	if (!::g_FBO_Pass1_G_Buffer.init(1920, 1080, error))
	{
		std::cout << "::g_FBO_Pass2_Deferred error: " << error << std::endl;
	}

	//if (!::g_FBO_Pass1_G_Buffer2.init(1920, 540, error))
	//{
	//	std::cout << "::g_FBO_Pass2_Deferred error: " << error << std::endl;
	//}

	if (!::g_FBO_Pass2_Deferred.init(1100, 700, error))
	{
		std::cout << "error: " << error << std::endl;
	}

	if (!::g_FBO_Pass2_Deferred2.init(1100, 670, error))
	{
		std::cout << "error: " << error << std::endl;
	}

	//if (!::g_FBO_Pass1_G_Buffer2.init(1920, 1080, error))
	if (!::defferedTexture.init(1100, 700, error))
	{
		std::cout << "error: " << error << std::endl;
	}

	if (!::defferedTexture2.init(1100, 670, error))
	{
		std::cout << " error: " << error << std::endl;
	}

	//if (!::g_FBO_Pass2_Deferred2.init(1920, 1080, error))
	//{
	//	std::cout << "::g_FBO_Pass2_Deferred error: " << error << std::endl;
	//}



	setWindowFullScreenOrWindowed(::g_pGLFWWindow, ::g_IsWindowFullScreen);

	::g_pTheCamera->targetObject = ::g_vecGameObjects[0];


	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();

	setAnimationStates();

	onlyTheFirstObject.push_back(::g_vecGameObjects[0]);


	onlyTheFourthObject.push_back(::g_vecGameObjects[5]);

	//everythingButTheThirdObject
	for (int i = 0; i < ::g_vecGameObjects.size(); ++i)
	{
		if (i != 3)
			everythingButTheThirdObject.push_back(::g_vecGameObjects[i]);
	}

	for (int i = 0; i < ::g_vecGameObjects.size(); ++i)
	{
		if (i != 4)
			everythingButTheFourthObject.push_back(::g_vecGameObjects[i]);
	}

	// Main game or application loop
	while (!glfwWindowShouldClose(::g_pGLFWWindow))
	{
		// Essentially the "frame time"
		// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;
		lastTimeStep = curTime;

		::gPhysicsWorld->TimeStep(deltaTime);

		g_pTheCamera->Update();

		::g_vecGameObjects[selectedGameObjectIndex]->UpdatePhys(deltaTime);

		cPhysicalProperties skyBoxPP;
		::g_pSkyBoxObject->GetPhysState(skyBoxPP);
		skyBoxPP.position = ::g_pTheCamera->getEyePosition();
		::g_pSkyBoxObject->SetPhysState(skyBoxPP);

		if (goingUp)
		{
			orbRadius += radiusStep;
			if (orbRadius > orbRadiusRandMax)
			{
				goingUp = false;
			}
		}
		else
		{
			orbRadius -= radiusStep;
			if (orbRadius < 0)
			{
				goingUp = true;
				xOrbLoc = getRandInRange(0.05f, 1.0f);
				yOrbLoc = getRandInRange(0.05f, 1.0f);

				orbRadiusRandMax = getRandInRange(0.05f, orbRadiusMax);
			}
		}


		if (goingUp2)
		{
			orbRadius2 += radiusStep;
			if (orbRadius2 > orbRadiusRandMax2)
			{
				goingUp2 = false;
			}
		}
		else
		{
			orbRadius2 -= radiusStep;
			if (orbRadius2 < 0)
			{
				goingUp2 = true;
				xOrbLoc2 = getRandInRange(0.05f, 1.0f);
				yOrbLoc2 = getRandInRange(0.05f, 1.0f);

				orbRadiusRandMax2 = getRandInRange(0.05f, orbRadiusMax);
			}
		}

		//    ___                _             _           ___       _            __   __           
		//   | _ \ ___  _ _   __| | ___  _ _  | |_  ___   / __| ___ | |__  _  _  / _| / _| ___  _ _ 
		//   |   // -_)| ' \ / _` |/ -_)| '_| |  _|/ _ \ | (_ ||___|| '_ \| || ||  _||  _|/ -_)| '_|
		//   |_|_\\___||_||_|\__,_|\___||_|    \__|\___/  \___|     |_.__/ \_,_||_|  |_|  \___||_|  
		//                                                                        
		// In this pass, we render all the geometry to the "G buffer"
		// The lighting is NOT done here. 
		// 
		//FirstPass(deltaTime);
		::g_pShaderManager->useShaderProgram("mySexyShader");

		// Direct everything to the FBO

		glUniform1i(renderPassNumber_LocID, RENDER_PASS_0_G_BUFFER_PASS);


		glBindFramebuffer(GL_FRAMEBUFFER, g_FBO_Pass1_G_Buffer.ID);
		// Clear colour AND depth buffer
		glEnable(GL_STENCIL_TEST);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glClearStencil(0);
		g_FBO_Pass1_G_Buffer.clearBuffers();



		GLint fullRenderedImage2D_LocID = glGetUniformLocation(sexyShaderID, "fullRenderedImage2D");
		/// Pick a texture unit... 
		unsigned int pass2unit = 50;
		glActiveTexture(GL_TEXTURE0 + pass2unit);
		glBindTexture(GL_TEXTURE_2D, ::g_FBO_Pass2_Deferred.colourTexture_0_ID);
		glUniform1i(fullRenderedImage2D_LocID, pass2unit);

		GLint fullRenderedImage2D_LocID2 = glGetUniformLocation(sexyShaderID, "fullRenderedImage2D2");
		/// Pick a texture unit... 
		unsigned int pass2unit2 = 51;
		glActiveTexture(GL_TEXTURE0 + pass2unit2);
		glBindTexture(GL_TEXTURE_2D, ::g_FBO_Pass2_Deferred2.colourTexture_0_ID);
		glUniform1i(fullRenderedImage2D_LocID2, pass2unit2);

		glStencilFunc(GL_ALWAYS, 1, 0xFF);
		glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
		glStencilMask(0xFF);
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glDepthMask(GL_TRUE);
		RenderScene(onlyTheFourthObject, ::g_pGLFWWindow, deltaTime);

		//glClear(GL_DEPTH_BUFFER_BIT);
		glStencilFunc(GL_EQUAL, 0, 0xFF);
		glStencilMask(0x00);
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glDepthMask(GL_TRUE);
		glStencilOp(GL_KEEP,		// Stencil fail
			GL_KEEP,		// Depth fail
			GL_KEEP);		// Stencil AND Depth PASS

		onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = false;


		RenderScene(::g_vecGameObjects, ::g_pGLFWWindow, deltaTime);


		onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = true;

		glStencilFunc(GL_EQUAL, 1, 0xFF);
		//glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);
		//glStencilMask(0x00);
		glDepthFunc(GL_GREATER);
		RenderScene(onlyTheFirstObject, ::g_pGLFWWindow, deltaTime);
		glDepthFunc(GL_LESS);
		//onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = false;
		glDisable(GL_STENCIL_TEST);

		////first pass for the second screen /////////////////////////////////////////////////////////////////
		//{
		//	glUniform1i(renderPassNumber_LocID, RENDER_PASS_0_G_BUFFER_PASS);


		//	glBindFramebuffer(GL_FRAMEBUFFER, g_FBO_Pass1_G_Buffer2.ID);
		//	// Clear colour AND depth buffer
		//	glEnable(GL_STENCIL_TEST);

		//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		//	glClearStencil(0);
		//	g_FBO_Pass1_G_Buffer2.clearBuffers();



		//	GLint fullRenderedImage2D_LocID = glGetUniformLocation(sexyShaderID, "fullRenderedImage2D");
		//	/// Pick a texture unit... 
		//	unsigned int pass2unit = 50;
		//	glActiveTexture(GL_TEXTURE0 + pass2unit);
		//	glBindTexture(GL_TEXTURE_2D, ::g_FBO_Pass2_Deferred.colourTexture_0_ID);
		//	glUniform1i(fullRenderedImage2D_LocID, pass2unit);

		//	glStencilFunc(GL_ALWAYS, 1, 0xFF);
		//	glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
		//	glStencilMask(0xFF);
		//	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		//	glDepthMask(GL_TRUE);
		//	RenderSceneV3(onlyTheFourthObject, ::g_pGLFWWindow, deltaTime);

		//	//glClear(GL_DEPTH_BUFFER_BIT);
		//	glStencilFunc(GL_EQUAL, 0, 0xFF);
		//	glStencilMask(0x00);
		//	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		//	glDepthMask(GL_TRUE);
		//	glStencilOp(GL_KEEP,		// Stencil fail
		//		GL_KEEP,		// Depth fail
		//		GL_KEEP);		// Stencil AND Depth PASS

		//	onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = false;


		//	RenderSceneV3(::g_vecGameObjects, ::g_pGLFWWindow, deltaTime);


		//	onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = true;

		//	glStencilFunc(GL_EQUAL, 1, 0xFF);
		//	//glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);
		//	//glStencilMask(0x00);
		//	glDepthFunc(GL_GREATER);
		//	RenderSceneV3(onlyTheFirstObject, ::g_pGLFWWindow, deltaTime);
		//	glDepthFunc(GL_LESS);
		//	//onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = false;
		//	glDisable(GL_STENCIL_TEST);
		//}

		//    ___         __                         _   ___                _             ___               
		//   |   \  ___  / _| ___  _ _  _ _  ___  __| | | _ \ ___  _ _   __| | ___  _ _  | _ \ __ _  ___ ___
		//   | |) |/ -_)|  _|/ -_)| '_|| '_|/ -_)/ _` | |   // -_)| ' \ / _` |/ -_)| '_| |  _// _` |(_-<(_-<
		//   |___/ \___||_|  \___||_|  |_|  \___|\__,_| |_|_\\___||_||_|\__,_|\___||_|   |_|  \__,_|/__//__/
		//   
		glBindFramebuffer(GL_FRAMEBUFFER, 0); //switch this to zero and it should render the scene
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		::g_pShaderManager->useShaderProgram("mySexyShader");

		glUniform1i(renderPassNumber_LocID, RENDER_PASS_1_DEFERRED_RENDER_PASS);

		GLint xLocOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "xLocOfOrb");
		glUniform1f(xLocOfOrb_UniLoc, xOrbLoc);

		GLint yLocOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "yLocOfOrb");
		glUniform1f(yLocOfOrb_UniLoc, yOrbLoc);

		GLint radiusOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "radius");
		glUniform1f(radiusOfOrb_UniLoc, orbRadius);

		GLint drunkEffect_UniLoc = glGetUniformLocation(sexyShaderID, "drunkEffect");
		glUniform1f(drunkEffect_UniLoc, drunkEffect);


		GLint xLocOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "xLocOfOrb2");
		glUniform1f(xLocOfOrb_UniLoc2, xOrbLoc2);

		GLint yLocOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "yLocOfOrb2");
		glUniform1f(yLocOfOrb_UniLoc2, yOrbLoc2);

		GLint radiusOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "radius2");
		glUniform1f(radiusOfOrb_UniLoc2, orbRadius2);


		//uniform sampler2D texFBONormal2D;
		//uniform sampler2D texFBOVertexWorldPos2D;

		GLint texFBOColour2DTextureUnitID = 10;
		GLint texFBOColour2DLocID = glGetUniformLocation(sexyShaderID, "texFBOColour2D");
		GLint texFBONormal2DTextureUnitID = 11;
		GLint texFBONormal2DLocID = glGetUniformLocation(sexyShaderID, "texFBONormal2D");
		GLint texFBOWorldPosition2DTextureUnitID = 12;
		GLint texFBOWorldPosition2DLocID = glGetUniformLocation(sexyShaderID, "texFBOVertexWorldPos2D");

		GLint texFBOColour2DTextureUnitID2 = 13;
		GLint texFBOColour2DLocID2 = glGetUniformLocation(sexyShaderID, "texFBOColour2D2");
		GLint texFBONormal2DTextureUnitID2 = 14;
		GLint texFBONormal2DLocID2 = glGetUniformLocation(sexyShaderID, "texFBONormal2D2");
		GLint texFBOWorldPosition2DTextureUnitID2 = 15;
		GLint texFBOWorldPosition2DLocID2 = glGetUniformLocation(sexyShaderID, "texFBOVertexWorldPos2D2");

		// Pick a texture unit... 
		glActiveTexture(GL_TEXTURE0 + texFBOColour2DTextureUnitID);
		glBindTexture(GL_TEXTURE_2D, g_FBO_Pass1_G_Buffer.colourTexture_0_ID);
		glUniform1i(texFBOColour2DLocID, texFBOColour2DTextureUnitID);

		glActiveTexture(GL_TEXTURE0 + texFBONormal2DTextureUnitID);
		glBindTexture(GL_TEXTURE_2D, g_FBO_Pass1_G_Buffer.normalTexture_1_ID);
		glUniform1i(texFBONormal2DLocID, texFBONormal2DTextureUnitID);

		glActiveTexture(GL_TEXTURE0 + texFBOWorldPosition2DTextureUnitID);
		glBindTexture(GL_TEXTURE_2D, g_FBO_Pass1_G_Buffer.vertexWorldPos_2_ID);
		glUniform1i(texFBOWorldPosition2DLocID, texFBOWorldPosition2DTextureUnitID);

		////second screen stuff
		//glActiveTexture(GL_TEXTURE0 + texFBOColour2DTextureUnitID2);
		//glBindTexture(GL_TEXTURE_2D, g_FBO_Pass1_G_Buffer2.colourTexture_0_ID);
		//glUniform1i(texFBOColour2DLocID2, texFBOColour2DTextureUnitID2);

		//glActiveTexture(GL_TEXTURE0 + texFBONormal2DTextureUnitID2);
		//glBindTexture(GL_TEXTURE_2D, g_FBO_Pass1_G_Buffer2.normalTexture_1_ID);
		//glUniform1i(texFBONormal2DLocID2, texFBONormal2DTextureUnitID2);

		//glActiveTexture(GL_TEXTURE0 + texFBOWorldPosition2DTextureUnitID2);
		//glBindTexture(GL_TEXTURE_2D, g_FBO_Pass1_G_Buffer2.vertexWorldPos_2_ID);
		//glUniform1i(texFBOWorldPosition2DLocID2, texFBOWorldPosition2DTextureUnitID2);

		// Set the sampler in the shader to the same texture unit (20)

		glfwGetFramebufferSize(::g_pGLFWWindow, &width, &height);

		GLint screenWidthLocID = glGetUniformLocation(sexyShaderID, "screenWidth");
		GLint screenHeightLocID = glGetUniformLocation(sexyShaderID, "screenHeight");
		glUniform1f(screenWidthLocID, (float)width);
		glUniform1f(screenHeightLocID, (float)height);


		std::vector< cGameObject* >  vecCopy2ndPass;
		// Push back a SINGLE quad or GIANT triangle that fills the entire screen
		// Here we will use the skybox (as it fills the entire screen)
		vecCopy2ndPass.push_back(::g_pSkyBoxObject);

		RenderScene(vecCopy2ndPass, ::g_pGLFWWindow, deltaTime);

		///////////////////////////////////////////////////////////////////////////
		//To get the offscreen texture for mirror
		//////////////////////////////////////////////////////////////////////////////////////
		{
			glm::vec3 oldEye = ::g_pTheCamera->getEyePosition();
			glm::vec3 oldPos = ::g_pTheCamera->getTargetPosition();
			::g_pTheCamera->setEyePosition(glm::vec3(80, 20, 250));
			::g_pTheCamera->setTargPosition(glm::vec3(80, 20, 0));


			cPhysicalProperties skyBoxPP;
			::g_pSkyBoxObject->GetPhysState(skyBoxPP);
			skyBoxPP.position = ::g_pTheCamera->getEyePosition();
			::g_pSkyBoxObject->SetPhysState(skyBoxPP);

			::g_pShaderManager->useShaderProgram("mySexyShader");

			// Direct everything to the FBO

			glUniform1i(renderPassNumber_LocID, RENDER_PASS_0_G_BUFFER_PASS);

			glDisable(GL_STENCIL_TEST);
			glBindFramebuffer(GL_FRAMEBUFFER, defferedTexture.ID);
			// Clear colour AND depth buffer
			//glEnable(GL_STENCIL_TEST);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			glClearStencil(0);
			defferedTexture.clearBuffers();


			onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = false;
			RenderScene(::everythingButTheFourthObject, ::g_pGLFWWindow, deltaTime);
			onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = true;




			////end of first pass for mirror


			glBindFramebuffer(GL_FRAMEBUFFER, g_FBO_Pass2_Deferred.ID); //switch this to zero and it should render the scene
												  //g_FBO_Pass2_Deferred.clearBuffers();
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			g_FBO_Pass2_Deferred.clearBuffers();
			::g_pShaderManager->useShaderProgram("mySexyShader");

			glUniform1i(renderPassNumber_LocID, RENDER_PASS_1_DEFERRED_RENDER_PASS);

			GLint xLocOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "xLocOfOrb");
			glUniform1f(xLocOfOrb_UniLoc, xOrbLoc);

			GLint yLocOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "yLocOfOrb");
			glUniform1f(yLocOfOrb_UniLoc, yOrbLoc);

			GLint radiusOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "radius");
			glUniform1f(radiusOfOrb_UniLoc, orbRadius);

			GLint drunkEffect_UniLoc = glGetUniformLocation(sexyShaderID, "drunkEffect");
			glUniform1f(drunkEffect_UniLoc, drunkEffect);


			GLint xLocOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "xLocOfOrb2");
			glUniform1f(xLocOfOrb_UniLoc2, xOrbLoc2);

			GLint yLocOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "yLocOfOrb2");
			glUniform1f(yLocOfOrb_UniLoc2, yOrbLoc2);

			GLint radiusOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "radius2");
			glUniform1f(radiusOfOrb_UniLoc2, orbRadius2);


			//uniform sampler2D texFBONormal2D;
			//uniform sampler2D texFBOVertexWorldPos2D;

			GLint texFBOColour2DTextureUnitID = 16;
			GLint texFBOColour2DLocID = glGetUniformLocation(sexyShaderID, "texFBOColour2D");
			GLint texFBONormal2DTextureUnitID = 17;
			GLint texFBONormal2DLocID = glGetUniformLocation(sexyShaderID, "texFBONormal2D");
			GLint texFBOWorldPosition2DTextureUnitID = 18;
			GLint texFBOWorldPosition2DLocID = glGetUniformLocation(sexyShaderID, "texFBOVertexWorldPos2D");

			// Pick a texture unit... 
			glActiveTexture(GL_TEXTURE0 + texFBOColour2DTextureUnitID);
			glBindTexture(GL_TEXTURE_2D, defferedTexture.colourTexture_0_ID);
			glUniform1i(texFBOColour2DLocID, texFBOColour2DTextureUnitID);

			glActiveTexture(GL_TEXTURE0 + texFBONormal2DTextureUnitID);
			glBindTexture(GL_TEXTURE_2D, defferedTexture.normalTexture_1_ID);
			glUniform1i(texFBONormal2DLocID, texFBONormal2DTextureUnitID);

			glActiveTexture(GL_TEXTURE0 + texFBOWorldPosition2DTextureUnitID);
			glBindTexture(GL_TEXTURE_2D, defferedTexture.vertexWorldPos_2_ID);
			glUniform1i(texFBOWorldPosition2DLocID, texFBOWorldPosition2DTextureUnitID);

			// Set the sampler in the shader to the same texture unit (20)

			glfwGetFramebufferSize(::g_pGLFWWindow, &width, &height);

			GLint screenWidthLocID = glGetUniformLocation(sexyShaderID, "screenWidth");
			GLint screenHeightLocID = glGetUniformLocation(sexyShaderID, "screenHeight");
			glUniform1f(screenWidthLocID, (float)width);
			glUniform1f(screenHeightLocID, (float)height);


			std::vector< cGameObject* >  vecCopy2ndPass;
			// Push back a SINGLE quad or GIANT triangle that fills the entire screen
			// Here we will use the skybox (as it fills the entire screen)
			vecCopy2ndPass.push_back(::g_pSkyBoxObject);

			RenderScene(vecCopy2ndPass, ::g_pGLFWWindow, deltaTime);
			::g_pTheCamera->setEyePosition(oldEye);
			::g_pTheCamera->setTargPosition(oldPos);
		}
		/////////////////////////////////////////////////////////////////////////////////////////
		//to get the offscreen texture for the other mirror
		/////////////////////////////////////////////////////////////////////////////////////////
		{
			glm::vec3 oldEye = ::g_pTheCamera->getEyePosition();
			glm::vec3 oldPos = ::g_pTheCamera->getTargetPosition();
			::g_pTheCamera->setEyePosition(glm::vec3(18, 20, 250));
			::g_pTheCamera->setTargPosition(glm::vec3(18, 20, 0));


			cPhysicalProperties skyBoxPP;
			::g_pSkyBoxObject->GetPhysState(skyBoxPP);
			skyBoxPP.position = ::g_pTheCamera->getEyePosition();
			::g_pSkyBoxObject->SetPhysState(skyBoxPP);

			::g_pShaderManager->useShaderProgram("mySexyShader");

			// Direct everything to the FBO

			glUniform1i(renderPassNumber_LocID, RENDER_PASS_0_G_BUFFER_PASS);

			glDisable(GL_STENCIL_TEST);
			glBindFramebuffer(GL_FRAMEBUFFER, defferedTexture2.ID);
			// Clear colour AND depth buffer
			//glEnable(GL_STENCIL_TEST);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			glClearStencil(0);
			defferedTexture2.clearBuffers();


			onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = false;
			RenderScene(everythingButTheThirdObject, ::g_pGLFWWindow, deltaTime);
			onlyTheFirstObject[0]->vecMeshes[0].bDrawAsWireFrame = true;


			glBindFramebuffer(GL_FRAMEBUFFER, g_FBO_Pass2_Deferred2.ID); //switch this to zero and it should render the scene
																		//g_FBO_Pass2_Deferred.clearBuffers();
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			g_FBO_Pass2_Deferred2.clearBuffers();
			::g_pShaderManager->useShaderProgram("mySexyShader");

			glUniform1i(renderPassNumber_LocID, RENDER_PASS_1_DEFERRED_RENDER_PASS);

			GLint xLocOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "xLocOfOrb");
			glUniform1f(xLocOfOrb_UniLoc, xOrbLoc);

			GLint yLocOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "yLocOfOrb");
			glUniform1f(yLocOfOrb_UniLoc, yOrbLoc);

			GLint radiusOfOrb_UniLoc = glGetUniformLocation(sexyShaderID, "radius");
			glUniform1f(radiusOfOrb_UniLoc, orbRadius);

			GLint drunkEffect_UniLoc = glGetUniformLocation(sexyShaderID, "drunkEffect");
			glUniform1f(drunkEffect_UniLoc, drunkEffect);


			GLint xLocOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "xLocOfOrb2");
			glUniform1f(xLocOfOrb_UniLoc2, xOrbLoc2);

			GLint yLocOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "yLocOfOrb2");
			glUniform1f(yLocOfOrb_UniLoc2, yOrbLoc2);

			GLint radiusOfOrb_UniLoc2 = glGetUniformLocation(sexyShaderID, "radius2");
			glUniform1f(radiusOfOrb_UniLoc2, orbRadius2);


			//uniform sampler2D texFBONormal2D;
			//uniform sampler2D texFBOVertexWorldPos2D;

			GLint texFBOColour2DTextureUnitID = 19;
			GLint texFBOColour2DLocID = glGetUniformLocation(sexyShaderID, "texFBOColour2D");
			GLint texFBONormal2DTextureUnitID = 20;
			GLint texFBONormal2DLocID = glGetUniformLocation(sexyShaderID, "texFBONormal2D");
			GLint texFBOWorldPosition2DTextureUnitID = 21;
			GLint texFBOWorldPosition2DLocID = glGetUniformLocation(sexyShaderID, "texFBOVertexWorldPos2D");

			// Pick a texture unit... 
			glActiveTexture(GL_TEXTURE0 + texFBOColour2DTextureUnitID);
			glBindTexture(GL_TEXTURE_2D, defferedTexture2.colourTexture_0_ID);
			glUniform1i(texFBOColour2DLocID, texFBOColour2DTextureUnitID);

			glActiveTexture(GL_TEXTURE0 + texFBONormal2DTextureUnitID);
			glBindTexture(GL_TEXTURE_2D, defferedTexture2.normalTexture_1_ID);
			glUniform1i(texFBONormal2DLocID, texFBONormal2DTextureUnitID);

			glActiveTexture(GL_TEXTURE0 + texFBOWorldPosition2DTextureUnitID);
			glBindTexture(GL_TEXTURE_2D, defferedTexture2.vertexWorldPos_2_ID);
			glUniform1i(texFBOWorldPosition2DLocID, texFBOWorldPosition2DTextureUnitID);

			// Set the sampler in the shader to the same texture unit (20)

			glfwGetFramebufferSize(::g_pGLFWWindow, &width, &height);

			GLint screenWidthLocID = glGetUniformLocation(sexyShaderID, "screenWidth");
			GLint screenHeightLocID = glGetUniformLocation(sexyShaderID, "screenHeight");
			glUniform1f(screenWidthLocID, (float)width);
			glUniform1f(screenHeightLocID, (float)height);


			std::vector< cGameObject* >  vecCopy2ndPass;
			// Push back a SINGLE quad or GIANT triangle that fills the entire screen
			// Here we will use the skybox (as it fills the entire screen)
			vecCopy2ndPass.push_back(::g_pSkyBoxObject);

			RenderScene(vecCopy2ndPass, ::g_pGLFWWindow, deltaTime);
			::g_pTheCamera->setEyePosition(oldEye);
			::g_pTheCamera->setTargPosition(oldPos);
		}

		std::stringstream ssTitle;

		glfwSetWindowTitle(::g_pGLFWWindow, ssTitle.str().c_str());

		// "Presents" what we've drawn
		// Done once per scene 
		glfwSwapBuffers(::g_pGLFWWindow);
		glfwPollEvents();


	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(::g_pGLFWWindow);
	glfwTerminate();

	// 
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;

	//    exit(EXIT_SUCCESS);
	return 0;
}



void setWindowFullScreenOrWindowed(GLFWwindow* pTheWindow, bool IsFullScreen)
{
	// Set full-screen or windowed
	if (::g_IsWindowFullScreen)
	{
		// Find the size of the primary monitor
		GLFWmonitor* pPrimaryScreen = glfwGetPrimaryMonitor();
		const GLFWvidmode* pPrimMonVidMode = glfwGetVideoMode(pPrimaryScreen);
		// Set this window to full screen, matching the size of the monitor:
		glfwSetWindowMonitor(pTheWindow, pPrimaryScreen,
			0, 0,				// left, top corner 
			pPrimMonVidMode->width, pPrimMonVidMode->height,
			GLFW_DONT_CARE);	// refresh rate

		std::cout << "Window now fullscreen at ( "
			<< pPrimMonVidMode->width << " x "
			<< pPrimMonVidMode->height << " )" << std::endl;
	}
	else
	{
		// Make the screen windowed. (i.e. It's CURRENTLY full-screen)
		// NOTE: We aren't saving the "old" windowed size - you might want to do that...
		// HACK: Instead, we are taking the old size and mutiplying it by 75% 
		// (the thinking is: the full-screen switch always assumes we want the maximum
		//	resolution - see code above - but when we make that maximum size windowed,
		//  it's going to be 'too big' for the screen)
		GLFWmonitor* pPrimaryScreen = glfwGetPrimaryMonitor();
		const GLFWvidmode* pPrimMonVidMode = glfwGetVideoMode(pPrimaryScreen);

		// Put the top, left corner 10% of the size of the full-screen
		int topCornerTop = (int)((float)pPrimMonVidMode->height * 0.1f);
		int topCornerLeft = (int)((float)pPrimMonVidMode->width * 0.1f);
		// Make the width and height 75% of the full-screen resolution
		int height = (int)((float)pPrimMonVidMode->height * 0.75f);
		int width = (int)((float)pPrimMonVidMode->width * 0.75f);

		glfwSetWindowMonitor(pTheWindow, NULL,		// This NULL makes the screen windowed
			topCornerLeft, topCornerTop,				// left, top corner 
			width, height,
			GLFW_DONT_CARE);	// refresh rate

		std::cout << "Window now windowed at ( "
			<< width << " x " << height << " )"
			<< " and offset to ( "
			<< topCornerLeft << ", " << topCornerTop << " )"
			<< std::endl;
	}
	return;
}

























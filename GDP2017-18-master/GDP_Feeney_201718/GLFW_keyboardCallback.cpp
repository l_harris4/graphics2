#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"

#include <iostream>

#include "cAnimationState.h"




bool isShiftKeyDown(int mods, bool bByItself = true);
bool isCtrlKeyDown(int mods, bool bByItself = true);
bool isAltKeyDown(int mods, bool bByItself = true);
bool areAllModifierKeysUp(int mods);
bool areAnyModifierKeysDown(int mods);

const float CAMERAROTATIONSPEED = 120;

extern std::string g_AnimationToPlay;
extern int selectedGameObjectIndex;

extern double g_CA_CountDown;// = 0.0f;

cAnimationState::sStateDetails jumpAnimation;
cAnimationState::sStateDetails walkAnimation;
cAnimationState::sStateDetails strafingLeftAnimation;
cAnimationState::sStateDetails strafingRightAnimation;
cAnimationState::sStateDetails leftRoll;
cAnimationState::sStateDetails rightRoll;
cAnimationState::sStateDetails runAnimation;
cAnimationState::sStateDetails attackAnimation;

void setAnimationStates()
{
	jumpAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Jump(FBX2013).FBX";
	//jumpAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->FindAnimationTotalTime(jumpAnimation.name);
	jumpAnimation.totalTime = 1.0f;
	jumpAnimation.frameStepTime = 0.01f;
	jumpAnimation.cancelOnEnd = true;

	walkAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Walk(FBX2013).FBX";
	//jumpAnimation.totalTime = pTempGO->pSimpleSkinnedMesh->FindAnimationTotalTime(jumpAnimation.name);
	walkAnimation.totalTime = 0.8f;
	walkAnimation.frameStepTime = 0.005f;

	strafingLeftAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Strafe-Left(FBX2013).FBX";
	strafingLeftAnimation.totalTime = 0.8f;
	strafingLeftAnimation.frameStepTime = 0.005f;

	strafingRightAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Strafe-Right(FBX2013).FBX";
	strafingRightAnimation.totalTime = 0.8f;
	strafingRightAnimation.frameStepTime = 0.005f;

	leftRoll.name = "assets/modelsFBX/RPG-Character_Unarmed-Roll-Left(FBX2013).FBX";
	leftRoll.totalTime = 0.6f;
	leftRoll.cancelOnEnd = true;
	leftRoll.frameStepTime = 0.005f;

	rightRoll.name = "assets/modelsFBX/RPG-Character_Unarmed-Roll-Right(FBX2013).FBX";
	rightRoll.totalTime = 0.6f;
	rightRoll.cancelOnEnd = true;
	rightRoll.frameStepTime = 0.005f;

	runAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Run(FBX2013).FBX";
	runAnimation.totalTime = 0.8f;
	runAnimation.frameStepTime = 0.01f;

	attackAnimation.name = "assets/modelsFBX/RPG-Character_Unarmed-Attack-Kick-L1(FBX2013).FBX";
	attackAnimation.totalTime = 1.0f;
	attackAnimation.frameStepTime = 0.01f;
	attackAnimation.cancelOnEnd = true;
}



/*static*/ void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);


	// Fullscreen to windowed mode on the PRIMARY monitor (whatever that is)
	if (isAltKeyDown(mods, true) && key == GLFW_KEY_ENTER)
	{
		if (action == GLFW_PRESS)
		{
			::g_IsWindowFullScreen = !::g_IsWindowFullScreen;

			setWindowFullScreenOrWindowed(::g_pGLFWWindow, ::g_IsWindowFullScreen);

		}//if ( action == GLFW_PRESS )
	}//if ( isAltKeyDown(...

	const float CAMERASPEED = 10.0f;

	const float CAM_ACCELL_THRUST = 100.0f;


	if (isShiftKeyDown(mods, true))
	{
		if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.size() > 0 && ::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == walkAnimation.name)
		{
			::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
			::g_vecGameObjects[selectedGameObjectIndex]->running = true;
			::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(runAnimation);
		}
		else
		{
			switch (key)
			{
			case GLFW_KEY_1:
				::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.y *= 0.99f;	// less 1%
				break;
			case GLFW_KEY_2:
				::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.y *= 1.01f; // more 1%
				if (::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.y <= 0.0f)
				{
					::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.y = 0.001f;	// Some really tiny value
				}
				break;
			case GLFW_KEY_3:	// Quad
				::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.z *= 0.99f;	// less 1%
				break;
			case GLFW_KEY_4:	//  Quad
				::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.z *= 1.01f; // more 1%
				if (::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.z <= 0.0f)
				{
					::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.z = 0.001f;	// Some really tiny value
				}
				break;

				// Lights
				// CAMERA and lighting
			case GLFW_KEY_A:
				::g_vecGameObjects[selectedGameObjectIndex]->adjQOrientationEuler(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
				break;
			case GLFW_KEY_D:
				::g_vecGameObjects[selectedGameObjectIndex]->adjQOrientationEuler(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
				break;
			case GLFW_KEY_W:
				if (action == GLFW_PRESS)
				{
					if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == walkAnimation.name)
					{
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					}
					if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.empty())
					{
						::g_vecGameObjects[selectedGameObjectIndex]->running = true;
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(runAnimation);
					}

				}
				//the following is to break into a run from a walk
				else if (action == GLFW_REPEAT)
				{
					if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == walkAnimation.name)
					{
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
						::g_vecGameObjects[selectedGameObjectIndex]->running = true;
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(runAnimation);
					}
				}
				else if (action == GLFW_RELEASE)
				{
					//only cancel the current animation if is the walk animation
					if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.size() > 0 &&
						(::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == walkAnimation.name ||
							::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == runAnimation.name))
					{
						::g_vecGameObjects[selectedGameObjectIndex]->running = false;
						::g_vecGameObjects[selectedGameObjectIndex]->walking = false;
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					}
				}
				break;
			case GLFW_KEY_S:
				if (action == GLFW_PRESS)
				{
					runAnimation.frameStepTime = -0.005f;
					runAnimation.currentTime = runAnimation.totalTime;
					if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == walkAnimation.name)
					{
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					}
					if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.empty())
					{
						::g_vecGameObjects[selectedGameObjectIndex]->running = true;
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(runAnimation);
					}
				}
				else if (action == GLFW_RELEASE)
				{
					//only cancel the current animation if is the walk animation
					if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.size() > 0 &&
						(::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == walkAnimation.name ||
							::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == runAnimation.name))
					{
						::g_vecGameObjects[selectedGameObjectIndex]->running = false;
						::g_vecGameObjects[selectedGameObjectIndex]->walking = false;
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					}
				}

				break;
			case GLFW_KEY_G:
			{
				float angle = ::g_pLightManager->vecLights[selectedGameObjectIndex].getLightParamSpotPrenumAngleOuter();
				::g_pLightManager->vecLights[selectedGameObjectIndex].setLightParamSpotPrenumAngleOuter(angle + 0.01f);
			}
			break;
			case GLFW_KEY_SPACE:	// Jump
				if (action == GLFW_PRESS)
				{
					if (::g_vecGameObjects[selectedGameObjectIndex]->strafingLeft ||
						::g_vecGameObjects[selectedGameObjectIndex]->strafingRight)
					{
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
						::g_vecGameObjects[selectedGameObjectIndex]->rolling = true;
						if (::g_vecGameObjects[selectedGameObjectIndex]->strafingLeft)
							::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(leftRoll);
						else
							::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(rightRoll);
					}
					else
					{
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
						::g_vecGameObjects[selectedGameObjectIndex]->jumping = true;
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(jumpAnimation);
					}			
				}

				break;
			case GLFW_KEY_H:
			{
				float angle = ::g_pLightManager->vecLights[selectedGameObjectIndex].getLightParamSpotPrenumAngleOuter();
				::g_pLightManager->vecLights[selectedGameObjectIndex].setLightParamSpotPrenumAngleOuter(angle - 0.01f);
			}
			break;

			case GLFW_KEY_LEFT:
			{
				::g_vecGameObjects[selectedGameObjectIndex]->adjQOrientationEuler(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
			}
			break;
			case GLFW_KEY_RIGHT:
			{
				::g_vecGameObjects[selectedGameObjectIndex]->adjQOrientationEuler(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
			}
			break;
			};//switch (key)
		}
	}//if ( isShiftKeyDown(mods, true) )





	if (areAllModifierKeysUp(mods))
	{
		//	const float CAMERASPEED = 100.0f;
		switch (key)
		{
		case GLFW_KEY_1:
			::g_pTheCamera->setCameraMode(cCamera::MODE_FLY_USING_LOOK_AT);
			std::cout << "Camera now in " << ::g_pTheCamera->getCameraModeString() << std::endl;;
			break;
		case GLFW_KEY_2:
			::g_pTheCamera->setCameraMode(cCamera::MODE_FOLLOW);
			std::cout << "Camera now in " << ::g_pTheCamera->getCameraModeString() << std::endl;;
			break;
		case GLFW_KEY_3:
			::g_pTheCamera->setCameraMode(cCamera::MODE_MANUAL);
			std::cout << "Camera now in " << ::g_pTheCamera->getCameraModeString() << std::endl;;
			break;

			// CAMERA and lighting
		case GLFW_KEY_A:
			if (action == GLFW_PRESS)
			{
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.empty())
				{
					::g_vecGameObjects[selectedGameObjectIndex]->strafingLeft = true;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(strafingLeftAnimation);
				}
			}
			else if (action == GLFW_RELEASE)
			{
				//only cancel the current animation if is the walk animation
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.size() > 0 &&
					(::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == strafingLeftAnimation.name))
				{
					::g_vecGameObjects[selectedGameObjectIndex]->walking = false;
					::g_vecGameObjects[selectedGameObjectIndex]->running = false;
					::g_vecGameObjects[selectedGameObjectIndex]->strafingLeft = false;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
				}
			}
			break;
		case GLFW_KEY_D:
			if (action == GLFW_PRESS)
			{
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.empty())
				{
					::g_vecGameObjects[selectedGameObjectIndex]->strafingRight = true;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(strafingRightAnimation);
				}
			}
			else if (action == GLFW_RELEASE)
			{
				//only cancel the current animation if is the walk animation
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.size() > 0 &&
					(::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == strafingRightAnimation.name))
				{
					::g_vecGameObjects[selectedGameObjectIndex]->walking = false;
					::g_vecGameObjects[selectedGameObjectIndex]->running = false;
					::g_vecGameObjects[selectedGameObjectIndex]->strafingRight = false;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
				}
			}
			break;
		case GLFW_KEY_W:
			if (action == GLFW_PRESS)
			{
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.empty())
				{
					::g_vecGameObjects[selectedGameObjectIndex]->walking = true;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(walkAnimation);
				}
			}
			else if (action == GLFW_RELEASE)
			{
				//only cancel the current animation if is the walk animation
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.size() > 0 &&
					(::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == walkAnimation.name ||
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == runAnimation.name))
				{
					::g_vecGameObjects[selectedGameObjectIndex]->walking = false;
					::g_vecGameObjects[selectedGameObjectIndex]->running = false;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
				}
			}
			break;
		case GLFW_KEY_S:
			if (action == GLFW_PRESS)
			{
				walkAnimation.frameStepTime = -0.005f;
				walkAnimation.currentTime = walkAnimation.totalTime;
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.empty())
				{
					::g_vecGameObjects[selectedGameObjectIndex]->walkingBackward = true;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(walkAnimation);
				}
			}
			else if (action == GLFW_RELEASE)
			{
				//only cancel the current animation if is the walk animation
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.size() > 0 &&
					(::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == walkAnimation.name ||
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name == runAnimation.name))
				{
					::g_vecGameObjects[selectedGameObjectIndex]->walking = false;
					::g_vecGameObjects[selectedGameObjectIndex]->running = false;
					::g_vecGameObjects[selectedGameObjectIndex]->walkingBackward = false;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
				}
			}
			break;

		case GLFW_KEY_SPACE:	// Jump
			if (action == GLFW_PRESS)
			{
				if (::g_vecGameObjects[selectedGameObjectIndex]->strafingLeft ||
					::g_vecGameObjects[selectedGameObjectIndex]->strafingRight)
				{
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					::g_vecGameObjects[selectedGameObjectIndex]->rolling = true;
					if (::g_vecGameObjects[selectedGameObjectIndex]->strafingLeft)
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(leftRoll);
					else
						::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(rightRoll);
				}
				else
				{
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					::g_vecGameObjects[selectedGameObjectIndex]->jumping = true;
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(jumpAnimation);
				}
			}

			break;
		case GLFW_KEY_UP:
		{
			if (action == GLFW_PRESS)
			{
				if (::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.size() == 0 || ::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue[0].name != jumpAnimation.name)
				{
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.push_back(attackAnimation);
				}
				
			}
		}
			break;
		case GLFW_KEY_DOWN:
		{
			if (action == GLFW_PRESS)
			{
				if (selectedGameObjectIndex == 0)
				{
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					selectedGameObjectIndex = 1;
				}
				else
				{
					::g_vecGameObjects[selectedGameObjectIndex]->pAniState->vecAnimationQueue.clear();
					selectedGameObjectIndex = 0;
				}
				::g_pTheCamera->targetObject = ::g_vecGameObjects[selectedGameObjectIndex];
			}

		}
		break;
		case GLFW_KEY_LEFT: //Move the camera "backward"
		{
			::g_vecGameObjects[selectedGameObjectIndex]->adjQOrientationEuler(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
		}
		break;
		case GLFW_KEY_RIGHT: //Move the camera "backward"
		{
			::g_vecGameObjects[selectedGameObjectIndex]->adjQOrientationEuler(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
		}
		break;

		}//switch
	}//if (areAllModifierKeysUp(mods))

	// HACK: print output to the console
//	std::cout << "Light[selectedGameObjectIndex] linear atten: "
//		<< ::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.y << ", "
//		<< ::g_pLightManager->vecLights[selectedGameObjectIndex].attenuation.z << std::endl;
	return;
}



// Helper functions
bool isShiftKeyDown(int mods, bool bByItself /*=true*/)
{
	if (bByItself)
	{	// shift by itself?
		if (mods == GLFW_MOD_SHIFT) { return true; }
		else { return false; }
	}
	else
	{	// shift with anything else, too
		if ((mods & GLFW_MOD_SHIFT) == GLFW_MOD_SHIFT) { return true; }
		else { return false; }
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

bool isCtrlKeyDown(int mods, bool bByItself /*=true*/)
{
	if (bByItself)
	{	// shift by itself?
		if (mods == GLFW_MOD_CONTROL) { return true; }
		else { return false; }
	}
	else
	{	// shift with anything else, too
		if ((mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL) { return true; }
		else { return false; }
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

bool isAltKeyDown(int mods, bool bByItself /*=true*/)
{
	if (bByItself)
	{	// shift by itself?
		if (mods == GLFW_MOD_ALT) { return true; }
		else { return false; }
	}
	else
	{	// shift with anything else, too
		if ((mods & GLFW_MOD_ALT) == GLFW_MOD_ALT) { return true; }
		else { return false; }
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

bool areAllModifierKeysUp(int mods)
{
	if (isShiftKeyDown(mods)) { return false; }
	if (isCtrlKeyDown(mods)) { return false; }
	if (isAltKeyDown(mods)) { return false; }

	// All of them are up
	return true;
}//areAllModifierKeysUp()

bool areAnyModifierKeysDown(int mods)
{
	if (isShiftKeyDown(mods)) { return true; }
	if (isCtrlKeyDown(mods)) { return true; }
	if (isAltKeyDown(mods)) { return true; }
	// None of them are down
	return false;
}